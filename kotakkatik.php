<?php

class kotakkatik {
    private $papan = [
        [0, 1, 1, 7, 6, 4],
        [4, 6, 2, 8, 6, 1],
        [2, 1, 1, 1, 8, 4],
        [8, 7, 4, 9, 1, 1],
        [8, 8, 6, 7, 9, 2],
        [8, 8, 5, 2, 6, 0],
    ];

    private $solusiJalan = [];

    public function totalEnergi() {
        $total = 0;
        foreach ($this->temukanJalan() as $koordinat) {
            $total += $this->papan[$koordinat[0]][$koordinat[1]];
        }
        return $total;
    }

    public function temukanJalan() {
        $jarak = [];
        $visited = [];
        $pred = [];

        for ($i = 0; $i < 6; $i++) {
            $jarak[$i] = array_fill(0, 6, INF);
            $visited[$i] = array_fill(0, 6, false);
            $pred[$i] = array_fill(0, 6, null);
        }

        $jarak[0][0] = 0;
        for ($count = 0; $count < 35; $count++) {
            $u = $this->cariJarakTerpendek($jarak, $visited);
            $visited[$u[0]][$u[1]] = true;

            for ($i = max(0, $u[0] - 1); $i <= min(5, $u[0] + 1); $i++) {
                for ($j = max(0, $u[1] - 1); $j <= min(5, $u[1] + 1); $j++) {
                    if (!$visited[$i][$j] && $jarak[$u[0]][$u[1]] + $this->papan[$i][$j] < $jarak[$i][$j]) {
                        $jarak[$i][$j] = $jarak[$u[0]][$u[1]] + $this->papan[$i][$j];
                        $pred[$i][$j] = $u;
                    }
                }
            }
        }

        $this->rekonstruksiJalan($jarak, $pred);
        return $this->solusiJalan['jalan'];
    }

    private function rekonstruksiJalan(array $jarak, array $pred) {
        $path = [];
        $u = [5, 5];

        while ($u != [0, 0]) {
            array_unshift($path, $u);
            $u = $pred[$u[0]][$u[1]];
        }

        array_unshift($path, [0, 0]);
        $totalNilai = 0;
        foreach ($path as $node) {
            $totalNilai += $this->papan[$node[0]][$node[1]];
        }

        $this->solusiJalan = ['jalan' => $path, 'nilai' => $totalNilai];
        return $path;
    }

    private function cariJarakTerpendek($jarak, $visited) {
        $minJarak = INF;
        $minNode = [-1, -1];

        for ($i = 0; $i < 6; $i++) {
            for ($j = 0; $j < 6; $j++) {
                if (!$visited[$i][$j] && $jarak[$i][$j] <= $minJarak) {
                    $minJarak = $jarak[$i][$j];
                    $minNode = [$i, $j];
                }
            }
        }

        return $minNode;
    }
}

/*
$m = new kotakkatik();
$j = $m->temukanJalan();
$en = $m->totalEnergi();
echo '<pre>';
print_r($j);
echo '<pre>';
print_r($en);
*/


