<?php

class punyahuruf {
    public function find($find, $src) {
        $arrFind = str_split($find);
        $arrScrOri = str_split($src);
        $arrScr = [];
        foreach ($arrScrOri as $perSrc) {
            $arrScr[strtoupper($perSrc)] = $perSrc;
        }

        $tempStat = [];
        foreach ($arrFind as $perFind) {
            if (isset($arrScr[strtoupper($perFind)])) {
                $tempStat[] = 'VALID';
            } else {
                $tempStat[] = 'INVALID';
            }
        }

        $res = TRUE;
        foreach ($tempStat as $perStat) {
            if ($perStat == 'INVALID') {
                $res = FALSE;
                break;
            }
        }

        return $res;
    }
}

/*
$ph = new punyahuruf();
$res1 = $ph->find('cat', 'antarctica');
$res2 = $ph->find('cat', 'australia');
$res3 = $ph->find('cat', 'ANTARCTICA');
echo '<br>';print_r($res1);
echo '<br>';print_r($res2);
echo '<br>';print_r($res3);
*/
