<?php

class kamus {

    public $pathstorage = 'storage/data.lf';

    public function add($text, $synom = []) {
        if ($synom) {
            $data = ['text' => $text, 'synom' => $synom];
            $save = $this->writeStorage($data);
            if ($save) {
                echo 'No 1. Data berhasil disimpan </br>';
            }
        } else {
            echo 'No 1. Sinonim harus diisi. </br>';
        }
    }

    public function findSynonym($findText) {
        $getData = $this->readStorage($this->pathstorage);
        if ($getData) {
            $preData = $merData = $merSynom = [];
            foreach ($getData as $perLine) {
                $toArr = json_decode($perLine, true);
                $preData[] = $toArr;
            }

            foreach ($preData as $perData) {
                $merData[$perData['text']][] = $perData['synom'];
            }

            foreach ($merData as $key => $items) {
                foreach ($items as $item) {
                    foreach ($item as $val) {
                        $merSynom[$key][] = $val;
                    }
                }
                $merSynom[$key][] = $key;
            }

            $getKey = [];
            foreach ($merSynom as $k => $arrText) {
                foreach ($arrText as $perText) {
                    $upText = strtoupper($perText);
                    if ($upText == strtoupper($findText)) {
                        $getKey[] = $k;
                    }
                }
            }

            $getKey = array_unique($getKey);
            $res = [];
            foreach ($merSynom as $i => $perSynom) {
                foreach ($getKey as $perKey) {
                    if ($i == $perKey) {
                        $res[] = $perSynom;
                    }
                }
            }

            $result = [];
            foreach ($res as $items) {
                foreach ($items as $item) {
                    if (strtoupper($findText) != strtoupper($item)) {
                        $result[] = $item;
                    }
                }
            }

            echo 'No 1. Sinonim dari "<b>'.$findText.'</b>"';
            $result = array_unique($result);
            return $result;

        } else {
            echo 'No 1. Data masih kosong </br>';
        }
    }

    public function writeStorage($data = []) {
        /*
         * $respath u/ path & filename
         * $data u/ array data yg di simpan
         * mode u/ mode insert data "append or replace data in file"
         */

        /*
         * make directory "mkdir storage"
         * set permission dir
         * "chmod 777 storage"
         * to can write file in dir
         */
        $this->checkDir('storage/');
        $respath = $this->pathstorage;

        /*
         * touch file
         * if not exist
         */

        if (!is_file($respath)) {
            $fp = fopen($respath, "wb");
        }

        $resdata = json_encode($data);

        /*
         * write json data
         */

        if ($resdata && $data) {
            $fp = fopen($respath, "a"); //opens file in append mode
            fwrite($fp, $resdata . "\n");
            fclose($fp);
            return true;
        }

        return false;
    }

    public function readStorage($respath) {
        $res = [];
        if (is_file($respath)) {
            $newrespath = $respath;

            /*
             * read make array
             * data file per line
             */

            if (is_file($newrespath)) {
                $fh = fopen($newrespath, 'r');
                while ($line = fgets($fh)) {
                    $res[] = $line;
                }
                fclose($fh);
            }
        }

        return $res;
    }

    public function checkDir($path) {
        $respath = false;
        if (is_dir($path)) {
            $respath = $path;
        } else {
            if (!mkdir($path, 0777, TRUE)) {
                throw new HttpRuntimeException(print_r('Failed Create Folder', true));
            }
            $respath = $path;
        }

        return $respath;
    }
}

/*
$kamus = new kamus();
$kamus->add('big', ['huge', 'more']);
$find = $kamus->findSynonym('large');
echo '<pre>';print_r($find);
*/