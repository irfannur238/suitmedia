<?php

require_once 'kamus.php';
require_once 'punyahuruf.php';
require_once 'kotakkatik.php';

/*
 * tes soal nomor 1
 */
echo '<p>Nomor 1.</p>';
$kamus = new kamus();
$kamus->add('huge', ['enormous', 'gigantic']);
$find = $kamus->findSynonym('huge');
echo '<pre>';print_r($find);



/*
 * tes soal nomor 2
 */
echo '<p>Nomor 2</p>';
$kk = new kotakkatik();
$jalan = $kk->temukanJalan();
$energi = $kk->totalEnergi();
var_dump($jalan);
var_dump($energi);



/*
 * tes soal nomor 3
 */
echo '<p>Nomor 3</p>';
$ph = new punyahuruf();
$res1 = $ph->find('cat', 'antarctica');
$res2 = $ph->find('cat', 'australia');
$res3 = $ph->find('cat', 'ANTARCTICA');
var_dump($res1);
var_dump($res2);
var_dump($res3);
